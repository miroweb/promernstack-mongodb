Antworten zur Aufgabe: Reading from MongoDB

Wenn "underline" gefolgt von "id" beim Objekt hinzugefügt und zurückgegeben wird, funktioniert das nur, wenn der Schreibvorgang korrekt war und das Objekt unverändert in die Datenbank eingesetzt wurde. Besser wäre es, die Ergebnisse direkt aus der Datenbank zu beziehen. Dies ist die sicherste Lösung.
