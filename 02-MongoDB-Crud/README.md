Antworten zur Aufgabe: MongoDB CRUD Operations

1. Gestützt auf die Dokumentation unter https://docs.mongodb.com/manual/tutorial/access-mongo-shell-help/ wird folgender Code benötigt:

db.employees.find({"name.middle": {$exists:true}})

2. Es handelt sich bei diesem Filter um einen JavaScript Code -> Die Anführungs- und Schlusszeichen können bei der Eigenschaft weggelassen werden (String).

3. "$unset" kann dazu verwendet werden, in einem Update ein Feld zu verschieben. Achtung: nicht damit verwechseln, wenn es auf «null» gesetzt wird!

4. Die Zahl "1" besteht in einer aufsteigenden Reihenfolge für den Index, während "-1" für eine herabsteigende Reihenfolge steht. Bei verbundenen Indexen kann das hilfreich sein -> es kann so in beide Richtungen gesucht werden.
