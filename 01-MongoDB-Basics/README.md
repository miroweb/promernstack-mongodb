Antwort zur Aufgabe: MongoDB Basics

In der Mongo Shell Dokumentation unter https://docs.mongodb.com/manual/tutorial/access-mongo-shell-help/ ist eine Methode help() aufgeführt. Mit db.collection.find() findet man Hilfe unter anderem zum "cursor"-Objekt. Nachdem ein curser-Objekt einer Variable zugeordnet wird und zwei mal die Tabulatortaste nach dem Variablennamen gefolgt von "." gedrückt wird, wird eine Liste von möglichen Methoden angezeigt.
