Antworten zur Aufgabe: Schema Initialization

1. Der Vorteil des Node.js Drivers: es gibt nur einen einheitlichen Weg, etwas über die Applikation der Scripts aufzubauen und mögliche Fehler zu vermeiden, da diese grösstenteils immer gleich aufgebaut sind. Voraussetzung ist die richtige Einrichtung aller Funktionen und der Node-Umgebung mit allen Modules.

2. Eine Suchleiste hilft bei der Suche nach Problemen bzw. Fehlern. In diesem Fall wäre ein «Test-Index» hilfreich um nach weiteren, hilfreichen Optionen zu suchen.
