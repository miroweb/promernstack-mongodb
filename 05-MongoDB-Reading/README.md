Antworten zur Aufgabe: Reading from MongoDB

1. Die Verbindung ist kein Objekt, sondern ein Verbindungs-Pool, welcher entscheidet, was der beste Weg ist, um einzelne Schritte auszuführen:

- z.B. die Verwendung globaler Variablen (= die üblichste und optimalste Vorgehensweise)

2. Angenommen, die Database steht für einen kurzen Zeitraum ( langer Zeitraum = bis zu 30 Sekunden) nicht zur Verfügung oder ist nicht erreichbar, versucht diese sich in diesem Zeitraum immer wieder erneut zu verbinden. Nach 30 Sekunden führt dies zu einem Error. Die Verbindung zum Server kann nun nicht wieder verbunden werden -> der Server muss neu gestartet werden.

3. Eine mögliche Variante: die Anwendung eines Limits auf die Ergebnisse mittels limit(), z.B. find().limit(100). Mit dieser Variante kann das Ergebnis auch im User Interface dargestellt werden und zwar mittels skip() So wird spezifiziert, wo die Ergebnisliste starten soll. find().limit(20) gibt die ersten 20 Dokumente zurück.

Eine andere Variante besteht in der Möglichkeit der Verwendung eines Dokuments nach dem anderen mittels hasNext() und/oder next(), um die Ergebnisse zum Client (Frontend) zu senden.
